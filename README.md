Código fonte da aplicação utilizada como exemplo no artigo [Como a Arquitetura Hexagonal pode diminuir o vendor lock-in em aplicações Serverless](https://gitlab.com/wisni/faas-blog-app/-/raw/master/docs/Como_a_Arquitetura_Hexagonal_pode_diminuir_o_vendor_lock-in_em_aplicacoes_Serverless.pdf?inline=false).

## Arquitetura

![Arquitetura](docs/arquitetura.png)
