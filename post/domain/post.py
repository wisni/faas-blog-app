import datetime
from infra.exceptions import BusinessException


class Post:

    def __init__(self, title, author, text):
        if not title:
            raise BusinessException("Title is required")
        if len(title) > 255:
            raise BusinessException("Title cannot be longer than 255 characters")
        if not author:
            raise BusinessException("Author is required")
        if not text:
            raise BusinessException("Text is required")

        self.title = title
        self.author = author
        self.text = text
        self.date = datetime.datetime.now()
