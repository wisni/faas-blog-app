import json
from app.insert_post_command import InsertPostCommand
from infra.google_firestore_adapter import GoogleFireStorePostRepository
from infra.exceptions import BusinessException

def google_cloudfunctions_handler(request):
    try:
        post_data = request.get_json()

        InsertPostCommand(GoogleFireStorePostRepository()).execute(post_data)
    except BusinessException as e:
        return str(e), 422
    return "", 204
