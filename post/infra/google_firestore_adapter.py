import firebase_admin
import hashlib
from firebase_admin import credentials
from firebase_admin import firestore
from domain.post import Post

# Use the application default credentials
cred = credentials.ApplicationDefault()
# cred = credentials.Certificate("/home/rodrigo/Downloads/level-night-275800-82db0e39701f.json")
firebase_admin.initialize_app(cred, {
    "projectId": "level-night-275800",
})


class GoogleFireStorePostRepository:

    def __init__(self):
        self._db = firestore.client()

    def add(self, post: Post):
        hash_id = hashlib.sha256(str(post.title + post.author).encode('utf-8')).hexdigest()
        doc_ref = self._db.collection("posts").document(hash_id)
        doc_ref.set({
            "title": post.title,
            "author": post.author,
            "text": post.text,
            "date": post.date
        })
