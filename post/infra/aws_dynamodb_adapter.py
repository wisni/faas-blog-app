import boto3
from domain.post import Post


class AWSDynamoDBPostRepository:

    def __init__(self):
        self._dynamodb = boto3.client("dynamodb", region_name="us-east-2")
        self._init_table()

    def _init_table(self):
        try:
            table = self._dynamodb.create_table(
                TableName="Posts",
                KeySchema=[
                    {
                        "AttributeName": "Title",
                        "KeyType": "HASH"
                    },
                    {
                        "AttributeName": "Author",
                        "KeyType": "RANGE"
                    }
                ],
                AttributeDefinitions=[
                    {
                        "AttributeName": "Title",
                        "AttributeType": "S"
                    },
                    {
                        "AttributeName": "Author",
                        "AttributeType": "S"
                    }
                ],
                ProvisionedThroughput={
                    "ReadCapacityUnits": 1,
                    "WriteCapacityUnits": 1
                }
            )

            table.meta.client.get_waiter('table_exists').wait(TableName='Posts')

            print("Table created successfully!")
        except self._dynamodb.exceptions.ResourceInUseException:
            pass
        except Exception as e:
            print("Error creating table:")
            print(e)

    def add(self, post: Post):
        self._dynamodb.put_item(
            TableName="Posts",
            Item={
                "Title": {"S": post.title},
                "Author": {"S": post.author},
                "Text": {"S": post.text},
                "Date": {"S": post.date.strftime("%Y-%m-%dT%H:%M:%S.%f%Z")}
            }
        )
