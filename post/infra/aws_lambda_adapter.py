import json
from app.insert_post_command import InsertPostCommand
from infra.aws_dynamodb_adapter import AWSDynamoDBPostRepository
from infra.exceptions import BusinessException


def lambda_handler(event, context):
    try:
        post_data = json.loads(event.get("body", None)) if event.get("body", None) else None

        InsertPostCommand(AWSDynamoDBPostRepository()).execute(post_data)
    except BusinessException as e:
        return {
            "statusCode": 422,
            "body": json.dumps(str(e))
        }

    return {
        "statusCode": 200,
        "body": json.dumps("Post successfully included")
    }
