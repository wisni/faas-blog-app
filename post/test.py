from infra.aws_lambda_adapter import lambda_handler
from infra.google_cloudfunctions_adapter import google_cloudfunctions_handler

# Google
from flask import Flask
from flask import request
app = Flask(__name__)

@app.route("/", methods=["POST"])
def flask_resource():
    return google_cloudfunctions_handler(request)

# AWS
if __name__ == "__main__":
    event = {"body": '{\n\t"title": "Teste de post do Postman",\n\t"author": "Rodrigo Wisnievski",\n\t"text": "Este é um teste de um post no meu blog"\n}'}
    lambda_handler(event, None)
