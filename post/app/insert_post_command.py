from domain.post import Post
from infra.exceptions import BusinessException


class InsertPostCommand:

    def __init__(self, repository):
        self._repository = repository

    def execute(self, post_data):
        if not post_data:
            raise BusinessException("Post data cannot be null")

        self._repository.add(Post(
            post_data.get("title", None),
            post_data.get("author", None),
            post_data.get("text", None),))
