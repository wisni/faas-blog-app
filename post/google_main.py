from infra.google_cloudfunctions_adapter import google_cloudfunctions_handler

def handler(request):
    return google_cloudfunctions_handler(request)
