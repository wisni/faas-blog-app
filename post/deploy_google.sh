#!/bin/bash

mkdir ../build
cp -r . ../build
cd ../build
py3clean .
mv google_requirements.txt requirements.txt
mv google_main.py main.py
rm test.py *.sh *.zip

gcloud functions deploy insert-post --runtime python37 --trigger-http --source=.

zip -r post_function_google.zip .
cp post_function_google.zip ../post
cd ../post
rm -rf ../build