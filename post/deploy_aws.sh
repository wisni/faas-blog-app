#!/bin/bash

mkdir ../build
cp -r . ../build
cd ../build
py3clean .
rm google_requirements.txt google_main.py test.py *.sh *.zip
zip -r post_function_aws.zip .
cp post_function_aws.zip ../post
cd ../post
rm -rf ../build

aws lambda update-function-code --function-name insert-post --zip-file fileb://post_function_aws.zip